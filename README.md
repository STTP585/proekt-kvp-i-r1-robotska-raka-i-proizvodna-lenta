# Proekt KVP i R1- Robotska raka i proizvodna lenta

Во овој проект идејата е роботската рака да земе дел (лего коцка) кога тој ќе биде детектиран од крајниот сензор на лентата. На гриперот од роботската рака е поставен сензор за боја и откако ќе биде земена коцката се чита која боја е таа. Во зависност од бојата, роботската рака извршува програмирани движења за да ја смести коцката во кутијата за соодветната боја.
