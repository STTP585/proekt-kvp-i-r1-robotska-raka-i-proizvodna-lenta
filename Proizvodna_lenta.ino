#include <Stepper.h>

#define prv 12 //prviot senzor za detekcija na kocka sto se dvizi
#define vtor 2 //vtoriot senzor za detekcija na kocka sto se dvizi
#define izlez 3 //digitalen pin koj se postavuva na visoko nivo togaš koga robotskata raka treba da ja zeme kockata na krajot na lentata

const int stepsPerRevolution = 32;
int x=0;

// IN1-IN3-IN2-IN4 
Stepper myStepper = Stepper(stepsPerRevolution, 8, 10, 9, 11);
Stepper myStepper2 = Stepper(stepsPerRevolution, 4, 6, 5, 7);

void setup() {
  
  Serial.begin(9600);
  pinMode(prv,INPUT);
  pinMode(vtor, INPUT);
  pinMode(izlez, OUTPUT);
  
}

void loop() {
  
  while(digitalRead(prv)==0 && digitalRead(vtor)==0 && x==0){
    myStepper.setSpeed(1100);
    myStepper.step(10);
    myStepper2.setSpeed(1000);
    myStepper2.step(-10); //dvata motori se dvizat zaedno no vo sprotivna nasoka bidejki se postaveni simetricno eden vo odnos na drug
    delay(2);
    digitalWrite(izlez, LOW); // "ne davaj signal na robotot deka treba da zeme del"
    Serial.println("vozi brzo");
  }
  if(digitalRead(prv)==1 && x==0){
    x=1;
    digitalWrite(izlez, LOW);
    Serial.println("Detektira prviot senzor");
  }
  if(digitalRead(vtor)){
    x=0;
    digitalWrite(izlez, HIGH); //dava signal na robotskata raka deka ima del za zemanje
    Serial.println("Detektira vtoriot senzor. STOP");
  }

  while(digitalRead(vtor)==0 && x==1){ //ima kocka megju dvata senzori ili prviot senzor togaš detektiral kocka
    myStepper.setSpeed(1000);
    myStepper.step(1);
    myStepper2.setSpeed(1000);
    myStepper2.step(-1); 
    delay(2);
    digitalWrite(izlez, LOW);
    Serial.println("vozi sporo");
  }
}
