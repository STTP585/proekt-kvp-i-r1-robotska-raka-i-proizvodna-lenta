#include <Servo.h>

#define S0 4
#define S1 2
#define S2 11
#define S3 7 //4 pinovi za senzorot za boja
#define sensorOut 8
#define t 10 

int redMin = 100; // Red minimum value
int redMax = 270; // Red maximum value
int greenMin = 100; // Green minimum value
int greenMax = 235; // Green maximum value
int blueMin = 100; // Blue minimum value
int blueMax = 270; // Blue maximum value


int redPW = 0;
int greenPW = 0;
int bluePW = 0;

int redValue;
int greenValue;
int blueValue;

int pos=0;
int x=1;
int color=0;

Servo baza;
Servo visina;
Servo dolzina;
Servo griper;

int i;

void setup() {
  
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(t,OUTPUT);
  pinMode(sensorOut, INPUT);
    
  baza.attach(3);
  visina.attach(5);
  dolzina.attach(6);
  griper.attach(9); //4 pinovi za site 4 servo motori

  Serial.begin(9600);  

  //home pozicija na robotot
  baza.write(112);
  visina.write(180);
  dolzina.write(0);
  griper.write(95);
  
}

void loop(){
  
  if(analogRead(A0)>700){ //analogno cita dali upravuvacot od lentata go postavil digitalniot pin 3 na visoko nivo i robotskata raka treba da zeme del
    //ako uslovot e ispolnet zapocnuva dviženjeto na robotot, vo sprotivno toj miruva vo Home položba
    
      for(pos=180;pos>=177;pos-=3){ //spushti ja visinata
        visina.write(pos);
        delay(70);
      }
      for(pos=0;pos<=55;pos+=3){ //odi vo dolzina do delo
        dolzina.write(pos);
        delay(70);
      }
      for(pos=90;pos>=48;pos-=3){ //soberi go gripero
          griper.write(pos);
          delay(70);
        }
      for(pos=177;pos<=180;pos+=3){ //kreni vo visina
        visina.write(pos);
        delay(70);
      } 
      for(pos=55;pos>=15;pos-=3){ //povleci dolzina
        dolzina.write(pos);
        delay(70);
      } 
  
      color=getcolor(); //procitaj boja
      Serial.println(color);
      
      if(color==0){ //ako nema ispolneto uslovi za definiranite boi, togas go pushta delot i odi vo Home
          for(pos=48;pos<=95;pos+=3){ //otpusti go delo
            griper.write(pos);
            delay(70);
          }
          for(pos=15;pos>=0;pos-=3){ //povleci dolzina
            dolzina.write(pos);
            delay(70);
          }
      }
      
      if(color==1){ //crvena
        color=0;
        for(pos=112;pos>=70;pos-=3){ //svrti do kutijata
            baza.write(pos);
            delay(60);
          }
          for(pos=48;pos<=95;pos+=3){ //otpusti go delo
            griper.write(pos);
            delay(70);
          }
          for(pos=15;pos>=0;pos-=3){ //povleci dolzina
            dolzina.write(pos);
            delay(70);
          }
          for(pos=70;pos<=112;pos+=3){ //vrati se so rotacijata vo home
            baza.write(pos);
            delay(60);
          }
      }
      
      if(color==2){//zelena
        color=0;
        for(pos=112;pos>=60;pos-=3){ //svrti do kutijata
            baza.write(pos);
            delay(60);
          }
        for(pos=15;pos<=95;pos+=4){ //odi vo dolzina do delo
            dolzina.write(pos);
            delay(70);
          }
        for(pos=48;pos<=95;pos+=3){ //otpusti go delo
            griper.write(pos);
            delay(70);
          }
        for(pos=95;pos>=0;pos-=4){ //povleci dolzina
            dolzina.write(pos);
            delay(70);
          }
        for(pos=60;pos<=112;pos+=3){ //vrati se vo so rotacijata vo home
            baza.write(pos);
            delay(60);
          }
      }
      
      if(color==3){//sina
        color=0;
        for(pos=112;pos>=80;pos-=3){ //svrti do kutijata
            baza.write(pos);
            delay(60);
          }
        for(pos=15;pos<=85;pos+=4){ //odi vo dolzina do delo
            dolzina.write(pos);
            delay(70);
          }
         for(pos=48;pos<=95;pos+=3){ //otpusti go delo
            griper.write(pos);
            delay(70);
          }
        for(pos=85;pos>=0;pos-=4){ //povleci dolzina
            dolzina.write(pos);
            delay(70);
          }
        for(pos=80;pos<=112;pos+=3){ //vrati se vo so rotacijata vo home
            baza.write(pos);
            delay(60);
          }
      }      
  }  
}

int getcolor() //funkcija za citanje na boja so senzorot za boja
{
  digitalWrite(S0,HIGH);
  digitalWrite(S1,LOW);
  redPW = getRedPW();
  redValue = map(redPW, redMin, redMax, 255, 0);
  delay(200);
  greenPW = getGreenPW();
  greenValue = map(greenPW, greenMin, greenMax, 255, 0);
  delay(200);
  bluePW = getBluePW();
  blueValue = map(bluePW, blueMin, blueMax, 255, 0);
  delay(200);
  Serial.print("Red = ");
  Serial.print(redValue);
  Serial.print(" - Green = ");
  Serial.print(greenValue);
  Serial.print(" - Blue = ");
  Serial.println(blueValue);
  if(redValue>300 && greenValue<335 && blueValue<335) //uslovi za da znae deka e procitana tocno taa boja
    return 1;
  else if(greenValue>290)
    return 2;
  else if(blueValue>300)
    return 3;  
  else 
    return 0;  
}

//tri pomoshni funkcii na funkcijata za citanje na boja
int getRedPW() {
  digitalWrite(S2,LOW);
  digitalWrite(S3,LOW);
  int PW;
  PW = pulseIn(sensorOut, LOW);
  return PW;
}

int getGreenPW() {
  digitalWrite(S2,HIGH);
  digitalWrite(S3,HIGH);
  int PW;
  PW = pulseIn(sensorOut, LOW);
  return PW;
}

int getBluePW() {
  digitalWrite(S2,LOW);
  digitalWrite(S3,HIGH);
  int PW;
  PW = pulseIn(sensorOut, LOW);
  return PW;
}
